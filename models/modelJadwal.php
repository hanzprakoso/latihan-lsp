<?php
require_once '../config/database.php';

class ModelJadwal extends Database{

    protected $conn;

    public function __construct()
    {
        $this->conn = $this->openDBConnection();
    }

    public function getJadwalByKelas($kelas)
    {
        $result = $this->conn->query("
            SELECT * FROM jadwal
            NATURAL JOIN kelas
            NATURAL JOIN mapel
            NATURAL JOIN guru
            WHERE nama_kelas = '$kelas'"
        );
        $data = array();
        while($row = $result->fetch(PDO::FETCH_ASSOC)){
            $data[] = $row;
        }
        // print_r($result);
        $this->closeDBConnection($this->conn);
        return $data;
        
    }

    public function postJadwalByKelas($data, $kelas)
    {
        $con = $this->openDbConnection();
        $sql = "INSERT INTO jadwal VALUES ('', :kelas, :mapel, :guru, :jamke)";
        $statement = $con->prepare($sql);
        foreach($data as $i => $v){
            if($v["mapel-".strval($i+1)] != ""){
                // $sql .= $i != 0 ? ", " : ""; 
                // $sql .= "('', '$kelas', '".$v["mapel-".strval($i+1)]."', '".$v["guru-".strval($i+1)]."', '".strval($i + 1)."' )";
                $statement->bindValue(':kelas', $kelas, PDO::PARAM_STR);
                $statement->bindValue(':mapel', $v["mapel-".strval($i+1)], PDO::PARAM_STR);
                $statement->bindValue(':guru', $v["guru-".strval($i+1)], PDO::PARAM_STR);
                $statement->bindValue(':jamke', strval($i+1), PDO::PARAM_INT);
                $statement->execute();
            }
            
        }
        // $sql = rtrim($sql, ',');
        // echo $sql;
        // try{
        //     $result = $con->query($sql);
        //     return $result;
        // } catch(PDOException $e){  
        //     echo $e->getMessage();  
        // }  
        $this->closeDBConnection($con);
    }
}


?>